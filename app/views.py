from flask import redirect
from flask import Flask
from flask import render_template
from flask import request
from flask import url_for
from flask_babel import Babel
from werkzeug.routing import BuildError

app = Flask(__name__)
babel = Babel(app)

SUPPORTED_LANGUAGES = ['en', 'es']

@babel.localeselector
def get_locale():
    return request.cookies.get('LANGUAGE', 'en')

@app.route('/')
def index():
    return render_template('index.html')
@app.route('/about-me')
def about():
    return render_template('about.html')
@app.route('/platform')
def platform():
    return render_template('platform.html')
@app.route('/media')
def media():
    return render_template('media.html')
@app.route('/contact')
def contact():
    return render_template('contact.html')

@app.route('/lang/<language>')
def select_language(language):
    url = '/'
    endpoint = request.args.get('camefrom')
    if endpoint is not None:
        try:
            url = url_for(endpoint)
        except BuildError:
            pass
    response = redirect(url)
    if language in SUPPORTED_LANGUAGES:
        response.set_cookie('LANGUAGE', language)
    return response

if __name__ == '__main__':
    app.run(debug=True)
