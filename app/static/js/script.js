$(document).ready(function() {
    var Menubtn = $('#btn-menu');
    var NavUl = $('#menu');

    $(window).scroll(function() {
        if ($(this).scrollTop() >= 350) {
            $('.wrapper nav').addClass('fixed');
        } else {
            $('.wrapper nav').removeClass('fixed');
        }
    });
    $(Menubtn).on('click', function() {
        $(NavUl).slideToggle();
    });
    $(window).resize(function() {
        if ($(document).width() > 978) {
            $(NavUl).css({
                'display': 'flex'
            });
        }
        if ($(document).width() < 978) {
            $(NavUl).css({
                'display': 'none'
            });
        }
    });
});
