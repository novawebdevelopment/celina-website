#Celina Benitez's Website

Celina is running for city counsel in Mount Rainier, Maryland.  This website
was created for her campaign.

## Setup

----------------
```bash
$ sudo apt install python3-venv
$ git clone git@bitbucket.org:novawebdevelopment/celina-website.git 
$ cd celina-website 
$ python3 -m venv venv
$ source venv/bin/activate
$ pip install -r requirements.txt
$ pybabel compile -d app/translations/
$ python run.py
```
----------------
